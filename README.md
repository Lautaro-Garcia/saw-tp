# TP Seguridad en Aplicaciones Web - UTN - FRBA

Grupos 02 y 03 -  2º Cuat. 2020

## Integrantes

Nombre Apellido| Legajo | Mail |
:---: | :---: | :---: |
Natán Szmedra | 159.010-8 | lszmedra@est.frba.utn.edu.ar
Brandon	Altmann	| 152.182-2 | baltmann@est.frba.utn.edu.ar
Sebastián Gabriel Guozden | 160.221-4 |sebaguozden@est.frba.utn.edu.ar
Osvaldo	Ceballos | 129.912-8 | oceballos@est.frba.utn.edu.ar
Matias Miguenz | 150592-0 | mmiguenz@est.frba.utn.edu.ar
Nahuel Oyhanarte | 154.566-8 | noyhanarte@est.frba.utn.edu.ar
Jhon Salazar | 153.944-9 | jsalazarburga@est.frba.utn.edu.ar
Aldo Silvestre | 153.511-0 | aldos@est.frba.utn.edu.ar
Erik Gervas	| 156.208-3 | erikgervas@est.frba.utn.edu.ar
Lautaro	García | 146.892-3 | laugarcia@est.frba.utn.edu.ar

## Accesos

* Jenkins: http://jenkins.saw-tp.tk
* Sonarqube: http://sonarqube.saw-tp.tk
* Demo PHP: http://demo.saw-tp.tk

## Instrucciones

Para desarrollo local:

* Clone: `git clone git@gitlab.com:Lautaro-Garcia/saw-tp.git`
* Build: `docker build -t demo-php:latest .`
* Run: `docker run -d -p 80:8000 demo-php:latest`
* Access: [http://127.0.0.1](127.0.0.1)

Para pushear Docker image desde local (ojo, lo hace Jenkins!):

* Tag: `docker tag demo-php:latest registry.gitlab.com/lautaro-garcia/saw-tp:latest`
* Login: `docker login registry.gitlab.com`
* Push: `docker push registry.gitlab.com/lautaro-garcia/saw-tp`
