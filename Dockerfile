FROM php:fpm-buster

RUN apt-get update && apt-get install git wget zip unzip -y 

# Install Symfony
RUN wget https://get.symfony.com/cli/installer -O - | bash
RUN mv /root/.symfony/bin/symfony /usr/local/bin/symfony
RUN ln -s /usr/local/bin/symfony /usr/local/bin/symfony-cmd

# Install Composer (to get dependencies)
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/master/web/installer -O - -q | php -- --quiet
RUN mv composer.phar /usr/local/bin/composer

ADD composer.json composer.lock ./
RUN composer install --prefer-dist
RUN composer clear-cache

ADD . .
RUN chown www-data var/*
EXPOSE 8000

ENTRYPOINT ["symfony", "server:start"]