final isThereAConexion( def host){    
   try {
        def url = new URL(host)         
        url.getContent()
        return true
    } catch(Exception ex) {
        return false
    }
}

pipeline {
    agent any
    
    environment {

        registryURL = 'https://registry.gitlab.com'
        registryRepo = 'registry.gitlab.com/lautaro-garcia/saw-tp'
        registryCredential = '615e56ba-b7ec-4c39-ba66-e70d284086b6'
        dockerImage = ''
        SHORT_COMMIT = "${GIT_COMMIT[0..7]}"
    
    }
    
    stages {

        stage('Test: Dependency Check') {
            steps {
                dependencycheck additionalArguments: ' --format ALL --noupdate', odcInstallation: 'dependencyCheck'
                dependencyCheckPublisher pattern: 'dependency-check-report.xml'
            }
        }

        stage('Test: SonarQube') {       
            environment {
                scannerHome = tool 'SonarQubeScanner'
                projectKey = 'saw-tp'
                exclusiones = '**/*report.*'
            }
            steps {
                withSonarQubeEnv('SonarQube') {
                    sh "${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=${projectKey} -Dsonar.exclusions=${exclusiones} "
                }
            }
        }

        stage('Result: Quality Gate') {
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        
        stage('Test: Snyk') {
            steps{
                script{
                    if(isThereAConexion('https://snyk.io')){
                        snykSecurity additionalArguments: '--debug --file=composer.lock', snykInstallation: 'Snyk', snykTokenId: 'snyk-token'
                    }else{
                        echo "Skipped, no connection to snyk.io"
                        currentBuild.result = 'UNSTABLE'
                    }
                }
            }
        }

        stage('Build: Create image') {
            steps {
                script {
                    dockerImage = docker.build registryRepo + ":$SHORT_COMMIT"
                }
            }
        }

        stage('Test: Snyk Docker') {
            steps{
                script{
                    if(isThereAConexion('https://snyk.io')){
                        snykSecurity failOnIssues: false, additionalArguments: "--debug --docker ${registryRepo}:${SHORT_COMMIT}", snykInstallation: 'Snyk', snykTokenId: 'snyk-token'
                    }else{
                        echo "Skipped, no connection to snyk.io"
                        currentBuild.result = 'UNSTABLE'
                    }
                }
            }
        }

        stage('Deploy: Push image') {
            when {
                branch 'master'
                equals expected: 'SUCCESS', actual: currentBuild.currentResult
            }
            steps {
                script{
                    docker.withRegistry(registryURL, '615e56ba-b7ec-4c39-ba66-e70d284086b6') {
                        dockerImage.push()
                        dockerImage.push("latest")
                    }
                }
            }
        }

        stage('Deploy: Update service') {
            when {
                branch 'master'
                equals expected: 'SUCCESS', actual: currentBuild.currentResult
            }
            steps {
                sh "chmod +x deploy.sh && ./deploy.sh"
            }
        }
    
    }

    post {
        always {
            sh "docker rmi ${registryRepo}:${SHORT_COMMIT}"
        }
    }

}
