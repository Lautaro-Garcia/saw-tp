# Update task definition
cat task-definition.json | jq --arg NEW "$registryRepo:$SHORT_COMMIT" '.containerDefinitions[0].image = $NEW' > task-definition-new.json

# Register task definition
aws ecs register-task-definition --family saw-tp --region us-east-1 --cli-input-json file://task-definition-new.json

# Update service
aws ecs update-service --region us-east-1 --cluster saw-tp --service saw-tp --task-definition saw-tp